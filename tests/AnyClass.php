<?php
/**
 * Created by PhpStorm.
 * User: Jan-Home
 * Date: 2018/10/20 0020
 * Time: 19:31
 */

class AnyClass
{
	const RET_STATIC_METHOD = 'static method return';
	
	const RET_PUBLIC_METHOD = 'public method return';
	
	const RET_PRIVATE_METHOD = 'private method return';
	
	public static function anyStaticMethod()
	{
		return self::RET_STATIC_METHOD;
	}
	
	public function anyPublicMethod()
	{
		return self::RET_PUBLIC_METHOD;
	}
	
	public function anyPrivateMethod()
	{
		return self::RET_PRIVATE_METHOD;
	}
	
	public function doSomeThing($input)
	{
		return $input;
	}
}